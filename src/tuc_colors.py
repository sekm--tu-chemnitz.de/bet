# colors from TU Chemnitz corporate design from
# https://www.tu-chemnitz.de/tu/pressestelle/cd/vorlagen.html#farben

darkgreen: str = '#005F50'
grey = '#70706F'
purple = '#A10B70'
darkblue = '#123375'
red = '#E4032D'
green = '#6A8A26'
darkred = '#9D0736'
orange = '#DF5F07'
blue = '#0075BF'
gold = '#8D6831'
silver = '#9D9C9C'
