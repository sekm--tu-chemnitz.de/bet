import pandas as pd
import matplotlib.pyplot as plt
from os import listdir
import os
from scipy.stats import linregress
import src.tuc_colors as tuc
import numpy as np
from mpltools import annotation
import uncertainties

pd.options.mode.chained_assignment = None  # default='warn'


def find_csv_filenames(path_to_dir: str, suffix='.csv') -> list:
    filenames = listdir(path_to_dir)
    return [filename for filename in filenames if filename.endswith(suffix)]


def read_overview(path_to_dir: str, suffix='.txt') -> str:
    filenames = listdir(path_to_dir)
    for file in filenames:
        if file.endswith(suffix):
            return pd.read_csv(path_to_dir+file, delim_whitespace=True)


"""def single_point_bet(df: pd.DataFrame, analysis_probs: pd.DataFrame) -> None:
    slope, intercept, r = get_slope_and_intercept(x=df['Relative Pressure'], y=df['1 / [ W((Po/P) - 1) ]'])

    sample_mass = float(analysis_props.mass)
    title = str(analysis_props.label)

    fig, ax = plt.subplots()
    ax.set_title(title)
    ax.scatter(x=df['Relative Pressure'], y=df['1 / [ W((Po/P) - 1) ]'], edgecolor=tuc.darkblue, facecolor=tuc.darkblue,
               label='measured data')
    ax.plot(df['Relative Pressure'], intercept + slope * df['Relative Pressure'], color=tuc.darkred,
            label='fitted line', linestyle='dotted')
    annotation.slope_marker((np.mean(ax.get_xlim()), np.mean(ax.get_ylim())), np.round(slope, 3))

    ax.set_xlabel(r'$\mathrm{Relative\ Pressure}\quad/\quad p/p_\mathrm{0}$')
    ax.set_ylabel(r'$1 / [ W((Po/P) - 1) ]$')
    ax.legend(loc='lower right', frameon=True)

    s_spec, s_total, cross_sectional_area, _, _, r, w_m, c = calculate_sa(df, sample_mass)

    textstr = '\n'.join((  # r'$W_\mathrm{m}=$'+fr'${w_m: .5g}$',
        fr'$C={c: .5g}$', fr'$s={slope: .5g}$', fr'$i={intercept: .5g}$', '-' * 25, fr'$r={r: .5g}$', '-' * 25,
        fr'$m={sample_mass: .5g}\ ' + r'\mathrm{g}$', fr'$A={s_total: .5g}\ ' + r'\mathrm{m^2}$',
        fr'$A/m={s_spec: .5g}\ ' + r'\mathrm{m^2/g}$'))

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle=None, facecolor='white', edgecolor='none', alpha=0.5)

    # place a text box in upper left in axes coords
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props)

    plt.show()
"""


def cm2inch(*tupl: tuple) -> tuple:
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i / inch for i in tupl[0])
    else:
        return tuple(i / inch for i in tupl)


def plot_bet(df: pd.DataFrame, analysis_props: pd.DataFrame) -> list:
    slope, intercept, r, slope_ulo, intercept_ulo, slope_uhi, intercept_uhi = get_slope_and_intercept(x=df['Relative Pressure'], y=df['1 / [ W((Po/P) - 1) ]'])

    sample_mass = float(analysis_props['mass[g]'])
    sample_mass_u = float(analysis_props['mass_u[%]'])
    title = str(analysis_props.label)

    file_container.update({title: {'x': df['Relative Pressure'],
                                   'y': df['1 / [ W((Po/P) - 1) ]'],
                                   'y_fit': intercept + slope * df['Relative Pressure']}})

    fig, ax = plt.subplots()
    ax.set_title(title)
    ax.scatter(x=df['Relative Pressure'], y=df['1 / [ W((Po/P) - 1) ]'], edgecolor=tuc.darkblue, facecolor=tuc.darkblue,
               label='measured data')
    ax.plot(df['Relative Pressure'], intercept + slope * df['Relative Pressure'], color=tuc.darkred,
            label='fitted line', linestyle='--')
    ax.plot(df['Relative Pressure'], intercept_ulo + slope_ulo * df['Relative Pressure'], color=tuc.red, linestyle=':')
    ax.plot(df['Relative Pressure'], intercept_uhi + slope_uhi * df['Relative Pressure'], color=tuc.red, linestyle=':')

    annotation.slope_marker((np.mean(ax.get_xlim()),
                             np.mean(ax.get_ylim())),
                             np.round(slope, 3))

    ax.set_xlabel(r'$\mathrm{Relative\ Pressure}\quad/\quad p/p_\mathrm{0}$')
    ax.set_ylabel(r'$1 / [ W((Po/P) - 1) ]$')
    ax.legend(loc='lower right', frameon=True)

    s_spec, s_total, cross_sectional_area, _, _, r, w_m, c, s_spec_ulo, s_spec_uhi = calculate_sa(df, sample_mass,
                                                                                                  sample_mass_u)

    textstr = '\n'.join([fr'$A/m={s_spec:.5g}±{np.abs((s_spec_uhi - s_spec_ulo) / 2):.5g}\ ' + r'\mathrm{m^2/g}$',
                         fr'$m={sample_mass:.5g}±{sample_mass * sample_mass_u/100:.5g}\ ' + r'\mathrm{g}$',
                         fr'$A={s_total:.5g}\ '+r'\mathrm{m^2}$',
                         '-' * 25,
                         fr'$C={c:.5g}$',
                         fr'$s={slope:.5g}$',
                         fr'$i={intercept:.5g}$',
                         '-' * 25,
                         fr'$r={r:.5g}$'])

    # these are matplotlib.patch.Patch properties

    props = dict(boxstyle=None, facecolor='None', edgecolor='None')

    # place a text box in upper left in axes coords
    ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=12, verticalalignment='top', bbox=props)

    plt.show()
    return s_spec, sample_mass, np.abs(s_spec_uhi - s_spec_ulo) / 2, file_container, r


def get_slope_and_intercept(x, y):
    slope, intercept, r, _, _ = linregress(x, y)
    slope_ulo, intercept_ulo, _, _, _ = linregress(x*(1+0.0015), y)
    slope_uhi, intercept_uhi, _, _, _ = linregress(x*(1-0.0015), y)
    return slope, intercept, r, slope_ulo, intercept_ulo, slope_uhi, intercept_uhi


def calculate_sa(df: pd.DataFrame, sample_mass: float, sample_mass_u: float):
    slope, intercept, r, slope_ulo, intercept_ulo, slope_uhi, intercept_uhi = get_slope_and_intercept(x=df['Relative Pressure'], y=df['1 / [ W((Po/P) - 1) ]'])
    if intercept < 0:
        print(f'Warning: Intercept of {intercept:.5g} smaller than 0.')

    avogadro_constant = 6.0221415E23
    molar_mass = 83.8
    cross_sectional_area = 20.5E-20

    w_m = 1/(slope+intercept)
    c = 1/(intercept*w_m)
    s_spec = w_m*avogadro_constant*cross_sectional_area/molar_mass
    s_total = s_spec*sample_mass

    w_m_ulo = 1 / (slope_ulo + intercept_ulo)
    s_spec_ulo = w_m_ulo * avogadro_constant * cross_sectional_area / molar_mass * (1+sample_mass_u/100)

    w_m_uhi = 1 / (slope_uhi + intercept_uhi)
    s_spec_uhi = w_m_uhi * avogadro_constant * cross_sectional_area / molar_mass * (1-sample_mass_u/100)

    if not 50 < c < 250:
        print(f'Warning: C {c:.5g} out of range from 50 to 250.')

    return s_spec, s_total, cross_sectional_area, slope, intercept, r, w_m, c, s_spec_ulo, s_spec_uhi


def write_results(path: str, label: str, mass: float, ssa: float, ssa_u: float, reg: float):
    head = '\t'.join(['Meas', 'mass[g]', 'ssa[m^2/g]', 'u(ssa)[m^2/g]', 'reg'])
    values = '\t'.join([label, f'{mass:.5g}', f'{ssa:.5g}', f'{ssa_u:.5g}', f'{reg:.5g}'])

    if not os.path.isfile(path+'results.txt'):
        f_res = open(path+'results.txt', 'w')
        f_res.write(head)
        f_res.write('\n')
        f_res.close()

    with open(path+'results.txt', 'a') as f_res:
        f_res.write(values)
        f_res.write('\n')


def plot_overview():
    results = pd.read_csv(path+'results.txt', delim_whitespace=True)

    results = results.sort_values(by=['ssa[m^2/g]'])
    labels = results['Meas']
    x = np.arange(0, len(labels), 1)

    fig, ax = plt.subplots(figsize=cm2inch(15.59, 7.7))

    plt.errorbar(x=x, y=results['ssa[m^2/g]'], yerr=results['u(ssa)[m^2/g]'], color=tuc.darkblue, marker='x',
                 linestyle='None')
    # You can specify a rotation for the tick labels in degrees or with keywords.
    plt.xticks(x, labels, rotation='45')
    # Pad margins so that markers don't get clipped by the axes
    plt.margins(0.2)
    # Tweak spacing to prevent clipping of tick-labels
    plt.subplots_adjust(bottom=0.15)
    plt.ylim(0)
    plt.xlabel('Sample')
    plt.ylabel(r'$\mathrm{BET\ Surface\ area\quad/\quad m^2\cdot g^{-1}}$')
    for i, txt in enumerate(results['ssa[m^2/g]']):
        ax.annotate(np.round(txt, 3), (x[i]-0.4, txt+1.75))
    plt.tight_layout()
    plt.savefig('overview.png')
    plt.show()


def plot_combined_regression(file_container):
    fig, ax = plt.subplots()
    for file in file_container:
        x = file_container[file]['x']
        y = file_container[file]['y']
        y_fit = file_container[file]['y_fit']

        ax.scatter(x=x, y=y, edgecolor=tuc.darkblue, facecolor=tuc.darkblue, label=file)
        ax.plot(x, y_fit, color=tuc.darkred, linestyle='--')
    plt.savefig('reg.png')
    plt.show()

temp_krypton = '77_K'
path = f'data/{temp_krypton}/'
overview = read_overview(path)
correction = False

if os.path.isfile(path + 'results.txt'):
    os.remove(path + 'results.txt')

file_container = {}
for file in find_csv_filenames(path):
    df = pd.read_csv(path+file)
    if correction:
        # correction rel press
        old_rel_press = df['Relative Pressure']
        new_rel_press = old_rel_press*12.642/13
        df['Relative Pressure'] = new_rel_press

        # correction y-axis
        df['1 / [ W((Po/P) - 1) ]'] = df['1 / [ W((Po/P) - 1) ]']*(old_rel_press-1)/(new_rel_press-1)

    analysis_props = overview[overview['csv'] == file]
    s_spec, sample_mass, s_spec_u, file_container, reg = plot_bet(df, analysis_props)

    write_results(path, label=analysis_props.iloc[0]['label'], mass=sample_mass, ssa=s_spec, ssa_u=s_spec_u, reg=reg)

plot_combined_regression(file_container)
plot_overview()
